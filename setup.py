#!/usr/bin/env python

import os
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))

packages = ['celery']

requires = []

about = {}
with open(os.path.join(here, 'celery', '__version__.py'), 'r') as f:
    exec(f.read(), about)

# The text of the README file
with open(os.path.join(here, 'README.md'), 'r') as f:
    readme = f.read()

setup(
    name=about['__title__'],
    version=about['__version__'],
    description=about['__description__'],
    long_description=readme,
    long_description_content_type="text/markdown",
    author=about['__author__'],
    author_email=about['__author_email__'],
    url=about['__url__'],
    packages=packages,
    package_data={'': ['LICENSE', 'NOTICE'], 'celery': ['app/*.py', 'apps/*.py', 'backends/*.py',
                                                        'backends/database/*.py', 'bin/*.py', 'concurrency/*.py',
                                                        'contrib/*.py', 'events/*.py', 'fixups/*.py', 'loaders/*.py',
                                                        'security/*.py', 'task/*.py', 'tests/*.py', 'tests/app/*.py',
                                                        'tests/backends/*.py', 'tests/bin/*.py', 'tests/bin/proj/*.py',
                                                        'tests/compat_modules/*.py', 'tests/concurrency/*.py',
                                                        'tests/contrib/*.py', 'tests/events/*.py', 'tests/fixups/*.py',
                                                        'tests/functional/*.py', 'tests/security/*.py',
                                                        'tests/slow/*.py', 'tests/tasks/*.py', 'tests/utils/*.py',
                                                        'tests/worker/*.py', 'utils/*.py', 'utils/dispatch/*.py',
                                                        'worker/*.py'
                                                        ]},
    package_dir={'celery': 'celery'},
    include_package_data=True,
    python_requires=">=3.6.*",
    install_requires=requires,
    license=about['__license__'],
    zip_safe=False,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'Natural Language :: English',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: Implementation :: CPython',
    ],
    project_urls={
        'Documentation': 'https://gitlab.com/Fer12/celery',
        'Source': 'https://gitlab.com/Fer12/celery',
    },
)
